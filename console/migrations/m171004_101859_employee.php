<?php

use yii\db\Migration;

class m171004_101859_employee extends Migration
{
    public function safeUp()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500),
            'mail' => $this->string(),
            'phone' => $this->string(),
            'date' => $this->date(),
            'post' => $this->string(500),
            'status' => $this->integer(),
            'org_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%employee}}');
    }
}
