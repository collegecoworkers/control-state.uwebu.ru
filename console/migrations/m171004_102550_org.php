<?php

use yii\db\Migration;

class m171004_102550_org extends Migration
{
    public function safeUp()
    {
        $this->createTable('org', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'date' => $this->date(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%org}}');
    }
}
