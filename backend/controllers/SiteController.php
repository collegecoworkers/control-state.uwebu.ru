<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;

use common\models\Employee;
use common\models\Org;
use common\models\User;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => ['class' => AccessControl::className(),
			'rules' => [
				[
					'actions' => [
						'signup',
						'login',
						'error'
					],
					'allow' => true,
				],
				[
					'actions' => [
						'logout',
						'index',
						'add-org',
						'update-org',
						'delete-org',
						'org',
						'down',
						'create',
						'update',
						'delete',
					],
					'allow' => true,
					'roles' => ['@'],
				],
			],
		],
	];
}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Org::find()->orderBy(['date' => SORT_ASC]);
		return $this->render('index', [
			'model' => $model
		]);
	}

	public function actionOrg($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();
		$org = Org::find()->where(['id' => $id])->one();
		$model = Employee::find()->where(['org_id' => $id]);
		return $this->render('org', [
			'org' => $org,
			'model' => $model,
		]);
	}

	public function actionCreate($id) {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Employee();

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Employee']['name'];
			$model->mail = Yii::$app->request->post()['Employee']['mail'];
			$model->phone = Yii::$app->request->post()['Employee']['phone'];
			$model->post = Yii::$app->request->post()['Employee']['post'];
			$model->org_id = $id;
			$model->date = date('Y-m-d');
			$model->save();
			return $this->redirect(['/site/org', 'id' => $id]);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Employee::allStatuses(),
			'orgs' => $this->getOrgs(),
		]);
	}

	public function actionUpdate($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Employee::findIdentity($id);

		if (Yii::$app->request->isPost) {
			$model->name = Yii::$app->request->post()['Employee']['name'];
			$model->mail = Yii::$app->request->post()['Employee']['mail'];
			$model->phone = Yii::$app->request->post()['Employee']['phone'];
			$model->post = Yii::$app->request->post()['Employee']['post'];
			$model->status = Yii::$app->request->post()['Employee']['status'];
			$model->save();
			return $this->redirect(['/site/org', 'id' => $model->org_id]);
		}

		return $this->render('create', [
			'model' => $model,
			'statuses' => Employee::allStatuses(),
			'orgs' => $this->getOrgs(),
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Employee::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionAddOrg() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Org();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Org']['title'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-org', [
			'model' => $model,
		]);
	}

	public function actionUpdateOrg($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = Org::findOne($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Org']['title'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-org', [
			'model' => $model,
		]);
	}

	public function actionDeleteOrg($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = Org::findOne($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				if($user->isAdmin($user['id'])) {
					Yii::$app->user->login($user);
					return $this->goHome();
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	public function getOrgs() {
		$orgs = [];
		$all_orgs = Org::find()->all();

		for ($i=0; $i < count($all_orgs); $i++) $orgs[$all_orgs[$i]->id] = $all_orgs[$i]->title;

		return $orgs;
	}

}
