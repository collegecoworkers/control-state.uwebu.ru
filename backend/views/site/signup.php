<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Регистрация';
$this->params['breadcrumbs'][] = $this->title;
?>
<br><br><br><br><br>
<div class="col-md-6 col-md-offset-3 ">
	<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

	<?= $form->field($model, 'username')->textInput(['autofocus' => true])->input('text', ['placeholder'=>'Логин']) ?>

	<?= $form->field($model, 'email')->textInput()->input('text', ['placeholder'=>'email']) ?>

	<?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder'=>'Пароль'])?>

	<div class="form-group">
		<?= Html::submitButton('Отправить', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
		<hr>
		<?= Html::a('Войти', ['/site/login'],['class' => 'btn']) ?>
	</div>

	<?php ActiveForm::end(); ?>
</div>
