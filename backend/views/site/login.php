<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<br><br><br><br><br>
<div class="row">
	<div class="col-md-6 col-md-offset-3 ">
		<?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

		<?= $form->field($model, 'email')->textInput(['autofocus' => true])
		->input('text', ['placeholder'=>'email']) ?>

		<?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder'=>'Password'])?>

		<div class="form-group">
			<?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
			<hr>
			<?= Html::a('Регистрация', ['/site/signup'],['class' => 'btn']) ?>
		</div>
		<?php ActiveForm::end(); ?>
	</div>
</div>
