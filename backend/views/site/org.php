<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', 'Работники');
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">
			<div class="progress-group">
			</div>
			<div class="contact-index">
					<?= Html::a(Yii::t('app','Добавить работника'), ['/site/create', 'id' => $_GET['id']]) ?>
				<div class="fa-br"></div>
				<br>
				<?php
				echo GridView::widget([
					'dataProvider' => $dataProvider,
					'layout' => "{items}\n{pager}",
					'columns' => [
						'name',
						'mail',
						'phone',
						'post',
						[
							'class' => 'yii\grid\ActionColumn',
							'header'=>'Действия', 
							'headerOptions' => ['width' => '80'],
							'template' => '{update} {delete}',
						],
					],
				]);
				?>

			</div>

		</div>
	</div>
</div>
