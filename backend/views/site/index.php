<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', 'Все организации');

$dataProvider = new ActiveDataProvider([
  'query' => $model,
  'pagination' => [
   'pageSize' => 20,
 ],
]);

?>

<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/site/create') ?>

<div class="fa-br"></div>
<?php
echo GridView::widget([
  'dataProvider' => $dataProvider,
  'layout' => "{items}\n{pager}",
  'columns' => [
    'title',
    [
      'class' => 'yii\grid\ActionColumn',
      'header'=>'Действия', 
      'headerOptions' => ['width' => '80'],
      'template' => '{update} {delete}',
      'buttons' => [
        'update' => function ($url, $model) {
          return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['site/update-org', 'id'=>$model->id ], [
            'title' => Yii::t('app', 'lead-update'),
          ]);
        },
        'delete' => function ($url, $model) {
          return Html::a('<span class="glyphicon glyphicon-trash"></span>', ['site/delete-org', 'id'=>$model->id ], [
            'title' => Yii::t('app', 'lead-delete'),
          ]);
        }

      ],
    ],
  ],
]);
?>
