<?php

use yii\helpers\Html;
use backend\components\widgets\ActiveForm;
use yii\widgets\MaskedInput;
use dosamigos\datetimepicker\DateTimePicker;

$this->title = Yii::t('app', 'Новая запись');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"></div>
		<div class="panel-body">

			<div class="link-update">
				<?php $form = ActiveForm::begin(); ?>

				<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
				<?= $form->field($model, 'mail')->textInput() ?>
				<?= $form->field($model, 'phone')->textInput() ?>
				<?= $form->field($model, 'post')->textInput() ?>

				<div class="form-group center">
					<?= Html::submitButton(Yii::t('app', 'Добавить'), ['class' =>  'btn btn-success']) ?>
				</div>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
	</div>
</div>
