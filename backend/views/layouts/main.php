<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use backend\assets\AppAsset;
use backend\assets\AdminLteAsset;

//AppAsset::register($this);
$asset      = AdminLteAsset::register($this);
$baseUrl    = $asset->baseUrl;
use backend\assets\FontAwesomeAsset;
FontAwesomeAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/admin/public/lib/bootstrap/css/bootstrap.css">
    
    <!-- Font Awesome -->
    <link rel="stylesheet" href="/admin/public/lib/font-awesome/css/font-awesome.css">
    
    <!-- Metis core stylesheet -->
    <link rel="stylesheet" href="/admin/public/css/main.css">
    
    <!-- metisMenu stylesheet -->
    <link rel="stylesheet" href="/admin/public/lib/metismenu/metisMenu.css">
    
    <!-- onoffcanvas stylesheet -->
    <link rel="stylesheet" href="/admin/public/lib/onoffcanvas/onoffcanvas.css">
    
    <!-- animate.css stylesheet -->
    <link rel="stylesheet" href="/admin/public/lib/animate.css/animate.css">



    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--For Development Only. Not required -->
<link rel="stylesheet" href="/admin/public/css/style-switcher.css">
<link rel="stylesheet" href="/admin/public/less/theme.css">
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.1/less.js"></script> -->
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <?php $this->beginBody() ?>

     <div class="bg-dark dk" id="wrap" style="min-height: calc(100vh - 48px);">
        <?= $this->render('header.php', ['baserUrl' => $baseUrl, 'title'=>Yii::$app->name]) ?>
        <?= $this->render('leftside.php', ['baserUrl' => $baseUrl]) ?>
        <?= $this->render('content.php', ['content' => $content]) ?>
        <?= $this->render('footer.php', ['baserUrl' => $baseUrl]) ?>
        <?= $this->render('rightside.php', ['baserUrl' => $baseUrl]) ?>
    </div>

    <footer class="Footer bg-dark dker">
      <p>2017 &copy; Metis Bootstrap Admin Template v2.4.2</p>
    </footer>
    <?php $this->endBody() ?>
        <!--jQuery -->
    <script src="/admin/public/lib/jquery/jquery.js"></script>
    <!--Bootstrap -->
    <script src="/admin/public/lib/bootstrap/js/bootstrap.js"></script>
    <!-- MetisMenu -->
    <script src="/admin/public/lib/metismenu/metisMenu.js"></script>
    <!-- onoffcanvas -->
    <script src="/admin/public/lib/onoffcanvas/onoffcanvas.js"></script>
    <!-- Screenfull -->
    <script src="/admin/public/lib/screenfull/screenfull.js"></script>
    <!-- Metis core scripts -->
    <script src="/admin/public/js/core.js"></script>
    <!-- Metis demo scripts -->
    <script src="/admin/public/js/app.js"></script>
    <script src="/admin/public/js/style-switcher.js"></script>
</body>
</html>
<?php $this->endPage() ?>
