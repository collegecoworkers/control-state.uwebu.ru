<div id="top">
	<!-- .navbar -->
	<nav class="navbar navbar-inverse navbar-static-top">
		<div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="topnav">
				<div class="btn-group">
					<a href="/admin/site/logout" data-toggle="tooltip" data-original-title="Logout" data-placement="bottom" class="btn btn-metis-1 btn-sm"><i class="fa fa-power-off"></i></a>
				</div>
			</div>
			<div class="collapse navbar-collapse navbar-ex1-collapse">
				<!-- .nav -->
				<ul class="nav navbar-nav">
					<li><a href="/">Управление штатом организации</a></li>
				</ul>
				<!-- /.nav -->
			</div>
		</div>
		<!-- /.container-fluid -->
	</nav>
</div>