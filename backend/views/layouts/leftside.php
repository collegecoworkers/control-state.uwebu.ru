<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Org;

$org = [];
$all_org = Org::find()->all();

for ($i=0; $i < count($all_org); $i++) {
	$item = $all_org[$i];
	$org[] = [
		'label' => $item->title,
		'icon' => 'fa fa-trello',
		'url' => "/admin/site/org?id=$item->id",
		'active' => ($this->context->route == 'site/org' and $_GET['id'] == $item->id)
	];
}
$org[] = [
	'label' => 'Добавить организацию',
	'icon' => 'fa fa-plus', 
	'url' => '/admin/site/add-org',
	'active' => $this->context->route == 'site/add-org'
];

?>
<div id="left">

	<!-- #menu -->
	<ul id="menu" class="bg-blue dker">
		<li class="nav-header">Menu</li>
		<li class="nav-divider"></li>
		<li class="">
			<a href="/"><i class="fa fa-dashboard"></i><span class="link-title">&nbsp;Главная</span>
			</a>
		</li>
		<li class="">
			<a href="javascript:;"><i class="fa fa-building "></i>
				<span class="link-title">Организации</span>
				<span class="fa arrow"></span>
			</a>
			<ul class="collapse">
				<?php foreach($org as $item): ?>
					<li class=" <?= (($item['active'] == 1) ? 'active' : '') ?>"><a href="<?= $item['url'] ?>"><i class="<?= $item['icon'] ?>"></i>&nbsp; <?= $item['label'] ?> </a></li>
				<?php endforeach ?>
			</ul>
		</li>
	</ul>
	<!-- /#menu -->
</div>