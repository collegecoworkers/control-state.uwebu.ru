<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use common\models\Folder;

class Employee extends ActiveRecord {

	public static function tableName() {
		return '{{%employee}}';
	}

	public function rules() {
		return [
			[['name'], 'string'],
			[['status'], 'integer'],
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Имя',
			'mail' => 'Почта',
			'phone' => 'Телефон',
			'post' => 'Должность',
			'status' => 'Статус',
			'date' => 'Дата',
			'org_id' => 'Проект',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function saveEmployee(){
		return $this->save();
	}

	public function getStatus(){
		switch ($this->status) {
			case 1: return 'Готова';
			case 0: return 'Не готова';
			default: return 'Ошибка';
		}
	}

	public static function allStatuses(){
		$statuses = [];
		$statuses[0] = 'Не готова';
		$statuses[1] = 'Готова';
		return $statuses;
	}

}
